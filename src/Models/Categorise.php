<?php

namespace src\Models;

use src\Services\Hydratation;

class Categorise {

    private int $TacheId;
    private int $CategoryId;

    use Hydratation;

    /**
     * Get the value of TacheId
     */
    public function getTacheId(): int
    {
        return $this->TacheId;
    }

    /**
     * Set the value of TacheId
     */
    public function setTacheId(int $TacheId): self
    {
        $this->TacheId = $TacheId;

        return $this;
    }

    /**
     * Get the value of CategoryId
     */
    public function getCategoryId(): int
    {
        return $this->CategoryId;
    }

    /**
     * Set the value of CategoryId
     */
    public function setCategoryId(int $CategoryId): self
    {
        $this->CategoryId = $CategoryId;

        return $this;
    }
}