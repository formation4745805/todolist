<?php

namespace src\Models;

use src\Services\Hydratation;

class Priority {

    private $PriorityId;
    private $PriorityName;

   use Hydratation;

    /**
     * Get the value of PriorityId
     */
    public function getPriorityId()
    {
        return $this->PriorityId;
    }

    /**
     * Set the value of PriorityId
     */
    public function setPriorityId($PriorityId): self
    {
        $this->PriorityId = $PriorityId;

        return $this;
    }

    /**
     * Get the value of PriorityName
     */
    public function getPriorityName()
    {
        return $this->PriorityName;
    }

    /**
     * Set the value of PriorityName
     */
    public function setPriorityName($PriorityName): self
    {
        $this->PriorityName = $PriorityName;

        return $this;
    }
}