<?php

namespace src\Models;

use src\Services\Hydratation;

class User {

    private $UserId;
    private $UserLastName;
    private $UserFirstName;
    private $UserMail;
    private $UserPassword;

    use Hydratation;

    /**
     * Get the value of UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

    /**
     * Set the value of UserId
     */
    public function setUserId($UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    /**
     * Get the value of UserLastName
     */
    public function getUserLastName()
    {
        return $this->UserLastName;
    }

    /**
     * Set the value of UserLastName
     */
    public function setUserLastName($UserLastName): self
    {
        $this->UserLastName = $UserLastName;

        return $this;
    }

    /**
     * Get the value of UserFirstName
     */
    public function getUserFirstName()
    {
        return $this->UserFirstName;
    }

    /**
     * Set the value of UserFirstName
     */
    public function setUserFirstName($UserFirstName): self
    {
        $this->UserFirstName = $UserFirstName;

        return $this;
    }

    /**
     * Get the value of UserMail
     */
    public function getUserMail()
    {
        return $this->UserMail;
    }

    /**
     * Set the value of UserMail
     */
    public function setUserMail($UserMail): self
    {
        $this->UserMail = $UserMail;

        return $this;
    }

    /**
     * Get the value of UserPassword
     */
    public function getUserPassword()
    {
        return $this->UserPassword;
    }

    /**
     * Set the value of UserPassword
     */
    public function setUserPassword($UserPassword): self
    {
        $this->UserPassword = $UserPassword;

        return $this;
    }
}