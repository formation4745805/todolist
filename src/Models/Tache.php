<?php

namespace src\Models;

use src\Services\Hydratation;

class Tache {

    private $TacheId;
    private $TacheTitle;
    private $TacheDescription;
    private $TacheDate;
    private $UserId;
    private $PriorityId;

    use Hydratation;

    /**
     * Get the value of TacheId
     */
    public function getTacheId()
    {
        return $this->TacheId;
    }

    /**
     * Set the value of TacheId
     */
    public function setTacheId($TacheId): self
    {
        $this->TacheId = $TacheId;

        return $this;
    }

    /**
     * Get the value of TacheTitle
     */
    public function getTacheTitle()
    {
        return $this->TacheTitle;
    }

    /**
     * Set the value of TacheTitle
     */
    public function setTacheTitle($TacheTitle): self
    {
        $this->TacheTitle = $TacheTitle;

        return $this;
    }

    /**
     * Get the value of TacheDescription
     */
    public function getTacheDescription()
    {
        return $this->TacheDescription;
    }

    /**
     * Set the value of TacheDescription
     */
    public function setTacheDescription($TacheDescription): self
    {
        $this->TacheDescription = $TacheDescription;

        return $this;
    }

    /**
     * Get the value of TacheDate
     */
    public function getTacheDate()
    {
        return $this->TacheDate;
    }

    /**
     * Set the value of TacheDate
     */
    public function setTacheDate($TacheDate): self
    {
        $this->TacheDate = $TacheDate;

        return $this;
    }

    /**
     * Get the value of UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

    /**
     * Set the value of UserId
     */
    public function setUserId($UserId): self
    {
        $this->UserId = $UserId;

        return $this;
    }

    /**
     * Get the value of PriorityId
     */
    public function getPriorityId()
    {
        return $this->PriorityId;
    }

    /**
     * Set the value of PriorityId
     */
    public function setPriorityId($PriorityId): self
    {
        $this->PriorityId = $PriorityId;

        return $this;
    }
}