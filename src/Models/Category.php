<?php

namespace src\Models;

use src\Services\Hydratation;

class Category {

    private $CategoryId;
    private $CategoryName;

   use Hydratation;

    /**
     * Get the value of CategoryId
     */
    public function getCategoryId()
    {
        return $this->CategoryId;
    }

    /**
     * Set the value of CategoryId
     */
    public function setCategoryId($CategoryId): self
    {
        $this->CategoryId = $CategoryId;

        return $this;
    }

    /**
     * Get the value of CategoryName
     */
    public function getCategoryName()
    {
        return $this->CategoryName;
    }

    /**
     * Set the value of CategoryName
     */
    public function setCategoryName($CategoryName): self
    {
        $this->CategoryName = $CategoryName;

        return $this;
    }
}