<?php

use src\Models\Database;
use src\Models\User;
use src\Repositories\UserRepository;

if(isset($_POST)){

    $data = file_get_contents("php://input");
    $user = (json_decode($data, true));
    $obj = new User($user);
    $id = $obj->getUserId();
    $database = new Database();
    $UserRepository = new UserRepository($database);

    if($UserRepository->deleteThisUserById($id)){
        echo "success";
        session_destroy();
        header("Location: index.php");
        exit;
    }
}