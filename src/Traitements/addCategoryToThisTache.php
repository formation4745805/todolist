<?php

use src\Models\Categorise;
use src\Models\Database;
use src\Repositories\CategoriseRepository;

if(isset($_POST)){
    $data = file_get_contents("php://input");
    $categorise = (json_decode($data, true));
    $obj = new Categorise($categorise);
    $TacheId = $obj->getTacheId();
    $CategoryId = $obj->getCategoryId();
    if(validateData($TacheId, $CategoryId)){
        $database = new Database();
        $categoriseRepository = new CategoriseRepository($database);
        if($categoriseRepository->addCategoryToThisTache($obj)){
            echo "success";
        } else {
            echo "le repo est faux";
        }
    }
}

function validateData ($TacheId, $CategoryId){
    if(isset($TacheId)){
        $TacheId = htmlspecialchars($TacheId);
        if(isset($CategoryId)){
            $CategoryId = htmlspecialchars($CategoryId);
            return true;
        }
    }
}