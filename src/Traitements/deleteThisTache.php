<?php

use src\Models\Database;
use src\Models\Tache;
use src\Repositories\TacheRepository;

if (isset($_POST)){

    $data = file_get_contents("php://input");
    $tache = (json_decode($data, true));
    $obj = new Tache($tache);
    $id = $obj->getTacheId();
    $database = new Database();
    $TacheRepository = new TacheRepository($database);

    if($TacheRepository->deleteThisTache($id)){
        echo "success";
    }
}