<?php

use src\Models\Database;
use src\Models\User;
use src\Repositories\UserRepository;

if(isset($_POST)){

    $data = file_get_contents("php://input");
    $user = (json_decode($data, true));
    $obj = new User($user);
    $id = $obj->getUserId();
    $lastName = $obj->getUserLastName();
    $firstName = $obj->getUserFirstName();
    $mail = $obj->getUserMail();
    $password = $obj->getUserPassword();
    if(validateData ($id, $lastName, $firstName, $mail, $password)){
        $database = new Database();
        $UserRepository = new UserRepository($database);
        if($UserRepository->updateThisUser($obj)){
            echo "success";
            } else {
                header('location:/?section=enregistrer&&erreur=ERREUR_MODIF_USER');
            }
    }
}

function validateData ($id, $lastName, $firstName, $mail, $password) {
    if(isset($id) && !empty($id)){
        $id = htmlspecialchars($id);
        if(isset($lastName) && !empty($lastName)){
            $lastName = htmlspecialchars($lastName);
            if(isset($firstName) && !empty($firstName)) {
                $firstName = htmlspecialchars($firstName);
                if(isset($mail) && !empty($mail)){
                    $mail = htmlspecialchars($mail);
                    if(isset($password) && !empty($password)){
                        $password = hash("whirlpool", $password);
                        return true;
                    } else {
                        header('location:/?section=enregistrer&&erreur=ERREUR_MDP_ABSENT');
                    }
                }else {
                    header('location:/?section=enregistrer&&erreur=ERREUR_EMAIL');
                }
            }else {
                header('location:/?section=enregistrer&&erreur=ERREUR_FIRSTNAME');
            }
        }else {
            header('location:/?section=enregistrer&&erreur=ERREUR_LASTNAME');
        }
    }
}