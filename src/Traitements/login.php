<?php

use src\Models\Database;
use src\Models\User;
use src\Repositories\UserRepository;



if (isset($_POST)) {
    $data = file_get_contents("php://input");
    $user = (json_decode($data, true));
    $obj = new User($user);
    $mail = $obj->getUserMail();
    $password = $obj->getUserPassword();
    if (validateLogin($mail, $password)){
        $DbConnexion = new Database();
        $UserRepository = new UserRepository($DbConnexion);

        if ($UserRepository->login($mail, $password)) {
            $_SESSION['connecté'] = TRUE;
            $_SESSION['user'] = serialize($UserRepository->getThisUserByMail($mail));
        }
    } else {
        $_SESSION['connecté'] = FALSE ;
    }
}

function validateLogin ($mail, $password){
    if (isset($mail) && !empty($mail)){
        $mail = htmlspecialchars($mail);
        if(isset($password) && !empty($password)){
            $password = hash("whirlpool", $password);
            return true;
        }
    }
}