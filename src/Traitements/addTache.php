<?php

use src\Models\Database;
use src\Models\Tache;
use src\Repositories\TacheRepository;

if(isset($_POST)){
    $data = file_get_contents("php://input");
    $tache = (json_decode($data, true));
    $obj = new Tache($tache);
    $title = $obj->getTacheTitle();
    $descri = $obj->getTacheDescription();
    $date = $obj->getTacheDate();
    $UserId = $obj->getUserId();
    $PriorityId = $obj->getPriorityId();
    if (validateData($title, $descri, $date, $UserId ,$PriorityId)){
        $database = new Database();
        $TacheRepository = new TacheRepository($database);

        if($TacheRepository->createThisTache($obj)){
        echo "success";
        }
    }


};

function validateData($title, $descri, $date, $UserId ,$PriorityId){
    if(isset($title)){
        $title = htmlspecialchars($title);
        if(isset($descri)){
            $descri = htmlspecialchars($descri);
            if(isset($date)){
                $date = htmlspecialchars($date);
                if(isset($UserId)){
                    $UserId = htmlspecialchars($UserId);
                    if(isset($PriorityId)){
                        $PriorityId = htmlspecialchars($PriorityId);
                        return true;
                    }
                }
            }
        }
    }
}
