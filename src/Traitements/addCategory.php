<?php

use src\Models\Category;
use src\Models\Database;
use src\Repositories\CategoryRepository;

if(isset($_POST)){
    $data = file_get_contents("php://input");
    $category = (json_decode($data, true));
    $obj = new Category($category);
    $database = new Database();
    $categoryRepository = new CategoryRepository($database);
    if ($categoryRepository->CreateThisCategory($obj)){
        echo "success";
    }
}