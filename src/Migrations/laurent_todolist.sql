-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 15 mars 2024 à 15:14
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `laurent_todolist`
--

-- --------------------------------------------------------

--
-- Structure de la table `todolist_categorise`
--

DROP TABLE IF EXISTS `todolist_categorise`;
CREATE TABLE IF NOT EXISTS `todolist_categorise` (
  `tache_id` int NOT NULL,
  `category_id` int NOT NULL,
  PRIMARY KEY (`tache_id`,`category_id`),
  KEY `Categoriser_Categorie0_FK` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todolist_category`
--

DROP TABLE IF EXISTS `todolist_category`;
CREATE TABLE IF NOT EXISTS `todolist_category` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todolist_priority`
--

DROP TABLE IF EXISTS `todolist_priority`;
CREATE TABLE IF NOT EXISTS `todolist_priority` (
  `priority_id` int NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todolist_tache`
--

DROP TABLE IF EXISTS `todolist_tache`;
CREATE TABLE IF NOT EXISTS `todolist_tache` (
  `tache_id` int NOT NULL AUTO_INCREMENT,
  `tache_title` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tache_description` varchar(250) NOT NULL,
  `tache_date` date NOT NULL,
  `user_id` int NOT NULL,
  `priority_id` int NOT NULL,
  PRIMARY KEY (`tache_id`),
  KEY `Tache_User_FK` (`user_id`),
  KEY `Tache_Priorite0_FK` (`priority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todolist_user`
--

DROP TABLE IF EXISTS `todolist_user`;
CREATE TABLE IF NOT EXISTS `todolist_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_lastName` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_firstName` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_mail` varchar(250) NOT NULL,
  `user_password` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `User_AK` (`user_mail`,`user_password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `todolist_categorise`
--
ALTER TABLE `todolist_categorise`
  ADD CONSTRAINT `Categoriser_Categorie0_FK` FOREIGN KEY (`category_id`) REFERENCES `todolist_category` (`category_id`),
  ADD CONSTRAINT `Categoriser_Tache_FK` FOREIGN KEY (`tache_id`) REFERENCES `todolist_tache` (`tache_id`);

--
-- Contraintes pour la table `todolist_tache`
--
ALTER TABLE `todolist_tache`
  ADD CONSTRAINT `Tache_Priorite0_FK` FOREIGN KEY (`priority_id`) REFERENCES `todolist_priority` (`priority_id`),
  ADD CONSTRAINT `Tache_User_FK` FOREIGN KEY (`user_id`) REFERENCES `todolist_user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
