<?php
namespace src\Repositories;

use src\Models\Database;
use src\Models\Category;
use PDO;
use PDOException;

class CategoryRepository {
  private $DB;

  public function __construct()
  {
    $database = new Database;
    $this->DB = $database->getDB();

    require_once __DIR__ . '/../../config.php';
  }

  public function getAllCategory(){
    $sql = "SELECT * FROM ".PREFIXE."category;";

    $retour = $this->DB->query($sql)->fetchAll(PDO::FETCH_CLASS, Category::class);

    return $retour;
  }

  public function getThisCategoryById(int $id): Category|bool {
    $sql = "SELECT * FROM ".PREFIXE."category WHERE category_id = :id";

    $statement = $this->DB->prepare($sql);
    $statement->bindParam(':id', $id);
    $statement->execute();
    $statement->setFetchMode(PDO::FETCH_CLASS, Category::class);
    $retour = $statement->fetch();

    return $retour;
  }

  public function getThisCategoryByName(string $name): Category|bool {
    $sql = "SELECT * FROM ". PREFIXE ."category WHERE category_name = :name";

    $statement = $this->DB->prepare($sql);
    $statement->bindParam(':name', $name);
    $statement->execute();
    $statement->setFetchMode(PDO::FETCH_CLASS, Category::class);
    return $statement->fetch();
  }

  public function CreateThisCategory(Category $Category): bool{
    $sql = "INSERT INTO ". PREFIXE . "category (category_id, category_name) VALUES (:ID, :NAME)";

    $statement = $this->DB->prepare($sql);

    $retour = $statement->execute([
      ':ID' => $Category->getCategoryId(),
      ':NAME' => $Category->getCategoryName()
    ]);

    return $retour;
  }

  public function UpdateThisCategory(Category $Category): bool{
    $sql = "UPDATE ". PREFIXE . "category 
            SET
              category_name = :NAME
            WHERE category_id = :ID";

    $statement = $this->DB->prepare($sql);

    $retour = $statement->execute([
      ':ID' => $Category->getCategoryId(),
      ':NAME' => $Category->getCategoryName()
    ]);

    return $retour;
  }

    public function deleteThisCategory(int $ID): bool {
      try{
      $sql = "DELETE FROM " . PREFIXE . "categorise WHERE category_id = :ID;
              DELETE FROM " . PREFIXE . "category WHERE category_id = :ID;";
  
      $statement = $this->DB->prepare($sql);
  
      return $statement->execute([':ID' => $ID]);
  
      } catch(PDOException $error) {
        echo "Erreur de suppression : " . $error->getMessage();
        return FALSE;
      }
    }

    public function getAllCategoryByTacheId (int $id) {

      $sql = "SELECT ".PREFIXE."category.category_name FROM ".PREFIXE."category JOIN ".PREFIXE."categorise
      WHERE ".PREFIXE."category.category_id = ".PREFIXE."categorise.category_id
      AND ".PREFIXE."categorise.tache_id = :id;";
  
      $statement = $this->DB->prepare($sql);
      $statement->execute([
          ":id"=>$id
      ]);
      $retour = $statement->fetchAll(PDO::FETCH_CLASS, Category::class);
  
      return $retour;
    }
}