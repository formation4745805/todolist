<?php

namespace src\Repositories;

use PDO;
use src\Models\Database;
use src\Models\Priority;

class PriorityRepository {

    private $DB;

    public function __construct() {
        $database = new Database;
        $this->DB = $database->getDB();

        require_once __DIR__ ."/../../config.php";
    }

    public function getAllPriority() {
        $sql = "SELECT * FROM ".PREFIXE."priority;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Priority::class);
        return $retour;
    }

    public function getThisPriorityById ($id) {
        $sql = "SELECT * FROM ".PREFIXE."priority WHERE priority_id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement-> execute([":id"=>$id]);
        $statement->setFetchMode(PDO::FETCH_CLASS, Priority::class);
        $retour = $statement->fetch();
        return $retour;
    }

    public function getThisPriorityByName ($name) {
        $sql = "SELECT * FROM ".PREFIXE."priority WHERE priority_name = :name;";
        $statement = $this->DB->prepare($sql);
        $statement-> execute([":name"=>$name]);
        $statement->setFetchMode(PDO::FETCH_CLASS, Priority::class);
        $retour = $statement->fetch();
        return $retour;
    }
}