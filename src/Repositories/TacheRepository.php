<?php

namespace src\Repositories;

use PDO;
use PDOException;
use src\Models\Database;
use src\Models\Tache;

class TacheRepository {

    private $DB;

    public function __construct()
    {
        $database = new Database;
        $this->DB = $database->getDB();

        require_once __DIR__ . "/../../config.php";
    }

    public function getAllTache() {
        $sql = "SELECT * FROM ".PREFIXE."tache;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Tache::class);
        return $retour;
    }

    public function getAllTacheByUserId ($id) {
        $sql = "SELECT * FROM ".PREFIXE."tache WHERE user_id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([":id"=>$id]);
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, Tache::class);
        return $retour;
    }

    public function getThisTachebyId($id) {
        $sql = "SELECT * FROM ".PREFIXE."tache WHERE tache_id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([":id"=>$id]);
        $statement->fetchMode(PDO::FETCH_CLASS, 'Tache');
        $retour = $statement->fetch();
        return $retour;
    }

    public function getAllTacheOrderByPriority() {
        $sql = "SELECT * FROM ".PREFIXE."tache ORDER BY priority_id DESC;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, 'Tache');
        return $retour;
    }

    public function getAllTacheOrderByDate() {
        $sql = "SELECT * FROM ".PREFIXE."tache ORDER BY tache_date;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, 'Tache');
        return $retour;
    }

    public function createThisTache(Tache $Tache) {
        $sql = "INSERT INTO ".PREFIXE."tache (tache_id, tache_title, tache_description, tache_date, user_id, priority_id) VALUES (:id, :title, :description, :date, :userId, :priorityId);";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":id" => $Tache->getTacheId(),
            ":title" => $Tache->getTacheTitle(),
            ":description" => $Tache->getTacheDescription(),
            ":date" => $Tache->getTacheDate(),
            ":userId" => $Tache->getUserId(),
            ":priorityId" => $Tache->getPriorityId()
        ]);
        return $retour;
    }

    public function updateThisTache(Tache $Tache) {
        $sql = "UPDATE ".PREFIXE."tache 
                SET
                    tache_title = :title,
                    tache_description = :description,
                    tache_date = :date,
                    user_id = :userId,
                    priority_id = :priorityId
                WHERE tache_id = :tache_id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":title"=>$Tache->getTacheTitle(),
            ":description" => $Tache->getTacheDescription(),
            ":date" => $Tache->getTacheDate(),
            ":userId" => $Tache->getUserId(),
            ":priorityId" => $Tache->getPriorityId(),
            ":tache_id" => $Tache->getTacheId()
        ]);
        return $retour;
    }

    public function deleteThisTache($id) {
        try {
            $sql = "DELETE FROM ".PREFIXE."categorise WHERE category_id = :id;
                    DELETE FROM ".PREFIXE."tache WHERE tache_id = :id;";
            $statement = $this->DB->prepare($sql);
            return $statement->execute([":id"=>$id]);
        } catch(PDOException $error) {
            echo "Erreur de suppression : " . $error->getMessage();
            return FALSE;
          }

    }
}