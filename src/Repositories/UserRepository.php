<?php

namespace src\Repositories;

use PDO;
use PDOException;
use src\Models\Database;
use src\Models\User;

class UserRepository {
    private $DB;

    public function __construct()
    {
        $database = new Database;
        $this->DB = $database->getDB();

        require_once __DIR__ . "/../../config.php";
    }

    public function getAllUser() {
        $sql = "SELECT * FROM ".PREFIXE."user;";
        $statement = $this->DB->prepare($sql);
        $statement->execute();
        $retour = $statement->fetchAll(PDO::FETCH_CLASS, User::class);
        return $retour;
    }

    public function getThisUserById($id) {
        $sql = "SELECT * FROM ".PREFIXE."user WHERE user_id = :id;";
        $statement = $this->DB->prepare($sql);
        $statement->execute([":id"=>$id]);
        $statement->fetchMode(PDO::FETCH_CLASS, User::class);
        $retour = $statement->fetch();
        return $retour;
    }

    public function getThisUserByMail($mail) {
        $sql = "SELECT * FROM ".PREFIXE."user WHERE user_mail = :mail;";
        $statement = $this->DB->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $statement->execute([":mail"=>$mail]);
        $retour = $statement->fetch();
        return $retour;
    }

    public function createThisUser(User $User) {
        $sql = "INSERT INTO ".PREFIXE."user VALUES ( NULL ,?,?,?,?);";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            $User->getUserLastName(),
            $User->getUserFirstName(),
            $User->getUserMail(),
            $User->getUserPassword()
        ]);
        return $retour;
    }

    public function updateThisUser (User $User) {
        $sql = "UPDATE ".PREFIXE."user 
                SET
                    user_lastName = :lastName,
                    user_firstName = :firstName,
                    user_mail = :mail,
                    user_password = :password
                WHERE user_id = :id;";
        $statement = $this->DB->prepare($sql);
        $retour = $statement->execute([
            ":lastName" => $User->getUserLastName(),
            ":firstName" => $User->getUserFirstName(),
            ":mail" => $User->getUserMail(),
            ":password" => $User->getUserPassword(),
            ":id" => $User->getUserId()
        ]);
        return $retour;
    }

    public function deleteThisUserById ($id) {
        try{
            $sql = "
            DELETE FROM ".PREFIXE."categorise
            WHERE ".PREFIXE."categorise.tache_id IN (
                SELECT tache_id FROM (
                    SELECT ".PREFIXE."categorise.tache_id
                    FROM ".PREFIXE."categorise
                    INNER JOIN ".PREFIXE."tache ON ".PREFIXE."categorise.tache_id = ".PREFIXE."tache.tache_id
                    INNER JOIN ".PREFIXE."user ON ".PREFIXE."tache.user_id = ".PREFIXE."user.user_id
                    WHERE ".PREFIXE."user.user_id = :id
                ) AS subquery
            );

            DELETE FROM ".PREFIXE."tache 
            WHERE ".PREFIXE."tache.user_id = :id;

            DELETE FROM ".PREFIXE."user 
            WHERE ".PREFIXE."user.user_id = :id;
                ";
            $statement = $this->DB->prepare($sql);
            return $statement->execute([":id"=>$id]);
        } catch(PDOException $error) {
            echo "Erreur de suppression : " . $error->getMessage();
            return FALSE;
        }
    }

    public function login($mail, $password){

        $hash = hash("whirlpool", $password);

        try{
            $sql = "SELECT * FROM ".PREFIXE."user WHERE user_mail = :mail AND user_password = :password ;";
            $statement = $this->DB->prepare($sql);
            $retour = $statement->execute([
                ":mail" => $mail,
                ":password" => $hash
            ]);
            return $retour;
        } catch(\PDOException $error){
            var_dump($error);
        }

        while ($row = $retour->fetch(\PDO::FETCH_ASSOC)){
            $user = new User($row);
        }

        if(isset($user)){
            return $retour->rowCount() == 1;
        }
    }
}