<?php

namespace src\Repositories;

use PDO;
use src\Models\Categorise;
use src\Models\Database;

class CategoriseRepository {

    private $DB;

    public function __construct()
  {
    $database = new Database;
    $this->DB = $database->getDB();

    require_once __DIR__ . '/../../config.php';
  }

  public function addCategoryToThisTache(Categorise $Categorise) {

    $sql = "INSERT INTO ".PREFIXE."categorise (tache_id, category_id) 
    VALUES (:tacheId , :categoryId);";
    $statement = $this->DB->prepare($sql);
    $retour = $statement->execute([
      ':tacheId' => $Categorise->getTacheId(),
      ':categoryId' => $Categorise->getCategoryId()
    ]);
    return $retour;
  }

}
