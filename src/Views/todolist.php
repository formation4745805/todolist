<?php

use src\Repositories\CategoryRepository;

$categoryRepository = new CategoryRepository;
$categoriesByTache = $categoryRepository->getAllCategoryByTacheId($tache->getTacheId());
$categories = $categoryRepository->getAllCategory();
$PriorityId = $PriorityRepository->getThisPriorityById($tache->getPriorityId());

?>
<div class="relative m-5 group overflow-hidden p-8 rounded-xl bg-white border border-gray-200 dark:border-gray-800 dark:bg-gray-900">
    <div aria-hidden="true" class="inset-0 absolute aspect-video border rounded-full -translate-y-1/2 group-hover:-translate-y-1/4 duration-300 bg-gradient-to-b from-fuchsia-700 to-white dark:from-white dark:to-white blur-2xl opacity-25 dark:opacity-5 dark:group-hover:opacity-10">
    </div>
    <div class="relative">
        <div class="border border-fuchsia-600/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-fuchsia-200 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
            <?= $tache->getTacheTitle() ?>
        </div>
        <div class="border border-<?php if($PriorityId->getPriorityId() == 3){?>red<?php }else if($PriorityId->getPriorityId() == 2){?>orange<?php }else if($PriorityId->getPriorityId() == 1){?>green<?php }?>-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-<?php if($PriorityId->getPriorityId() == 3){?>red<?php }else if($PriorityId->getPriorityId() == 2){?>orange<?php }else if($PriorityId->getPriorityId() == 1){?>green<?php }?>-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950">
        <?= $PriorityId->getPriorityName() ?>
        </div>

        <div class="mt-6 pb-6 rounded-b-[--card-border-radius]">
            <p class="text-gray-700 dark:text-gray-300">
                <?= $tache->getTacheDescription() ?>
            </p>
        </div>

        <div class="mt-2 pb-2 rounded-b-[--card-border-radius] text-gray-700 dark:text-gray-300">
            Catégories :
            <?php
            foreach($categoriesByTache as $category){ ?>
            <p class="border border-fuchsia-600/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-fuchsia-200 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-lg font-semibold text-gray-900 dark:text-white text-center">
                <?= $category->getCategoryName() ?>
            </p>
            <?php } ?>
        </div>

        <div class="flex gap-3 -mb-8 py-4 border-t border-gray-200 dark:border-gray-800">
            <button class="openModifTache group rounded-xl disabled:border *:select-none [&>*:not(.sr-only)]:relative *:disabled:opacity-20 disabled:text-gray-950 disabled:border-gray-200 disabled:bg-gray-100 dark:disabled:border-gray-800/50 disabled:dark:bg-gray-900 dark:*:disabled:!text-white text-gray-950 bg-gray-100 hover:bg-gray-200/75 active:bg-gray-100 dark:text-white dark:bg-gray-500/10 dark:hover:bg-gray-500/15 dark:active:bg-gray-500/10 flex gap-1.5 items-center text-sm h-8 px-3.5 justify-center">
                <span>Editer</span>
                <svg class="h-5 w-5 text-fuchsia-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M7 10h3v-3l-3.5 -3.5a6 6 0 0 1 8 8l6 6a2 2 0 0 1 -3 3l-6-6a6 6 0 0 1 -8 -8l3.5 3.5" /></svg>
            </button>
            <a onclick="deleteThisTache(<?=$tache->getTacheId()?>)" class="group flex items-center rounded-xl disabled:border *:select-none [&>*:not(.sr-only)]:relative *:disabled:opacity-20 disabled:text-gray-950 disabled:border-gray-200 disabled:bg-gray-100 dark:disabled:border-gray-800/50 disabled:dark:bg-gray-900 dark:*:disabled:!text-white text-gray-950 bg-gray-100 hover:bg-gray-200/75 active:bg-gray-100 dark:text-white dark:bg-gray-500/10 dark:hover:bg-gray-500/15 dark:active:bg-gray-500/10 size-8 justify-center">
                <span class="sr-only">Supprimer</span>
                <svg class="h-5 w-5 text-fuchsia-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <line x1="4" y1="7" x2="20" y2="7" />  <line x1="10" y1="11" x2="10" y2="17" />  <line x1="14" y1="11" x2="14" y2="17" />  <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />  <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
            </a>
            <div class="p-5 mb-4 ml-4 border border-gray-100 rounded-lg bg-gray-50 dark:bg-gray-800 dark:border-gray-700">
                <time class="text-lg font-semibold text-gray-900 dark:text-white"><?= $tache->getTacheDate() ?></time>
            </div>
            <div class="relative h-11 w-full min-w-[200px]">
                <select class="addCategoryToThisTache peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" ">
                    <?php
                    foreach($categories as $category){ ?>
                        <option value="<?= $category->getCategoryId(); ?>"><?= $category->getCategoryName(); ?> </option>
                    <?php } ?>
                </select>
                <label for="addCategoryToThisTache" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                    Ajouter une catégorie à cette tâche:
                </label>
                <button 
                onclick="handleCategoryToTache (<?=$tache->getTacheId()?>)"
                class=" group rounded-xl disabled:border *:select-none [&>*:not(.sr-only)]:relative *:disabled:opacity-20 disabled:text-gray-950 disabled:border-gray-200 disabled:bg-gray-100 dark:disabled:border-gray-800/50 disabled:dark:bg-gray-900 dark:*:disabled:!text-white text-gray-950 bg-gray-100 hover:bg-gray-200/75 active:bg-gray-100 dark:text-white dark:bg-gray-500/10 dark:hover:bg-gray-500/15 dark:active:bg-gray-500/10 flex gap-1.5 items-center text-sm h-8 px-3.5 justify-center">
                <span>Ajouter</span>
                </button>
            </div>
        </div>
    </div>
</div>
