<?php

require_once __DIR__ . '/../src/init.php';

use src\Repositories\PriorityRepository;
use src\Repositories\TacheRepository;

if (isset($_SESSION['connecté']) && isset($_SESSION['user'])) {

    $user = unserialize($_SESSION['user']);
    $taches = new TacheRepository;
    $tachesListe = $taches->getAllTacheByUserId($user->getUserId());
    $PriorityRepository = new PriorityRepository;
}

$code_erreur = null;
if (isset($_GET['erreur'])) {
    require __DIR__ . '/../src/Traitements/configErreur.php';
    $code_erreur = (int) $_GET['erreur'];
}

if (isset($_GET['req'])) {
    switch ($_GET['req']) {
        case 'register':
            require __DIR__ . '/../src/Traitements/register.php';
            break;
        case 'login':
            require __DIR__ . '/../src/Traitements/login.php';
            break;
        case 'addTache':
            require __DIR__ . '/../src/Traitements/addTache.php';
            break;
        case 'addCategory':
            require __DIR__ . '/../src/Traitements/addCategory.php';
            break;
        case 'disconnect':
            require __DIR__ . '/../src/Traitements/disconnect.php';
            break;
        case 'addOption':
            require __DIR__ . '/../src/Traitements/addCategoryToThisTache.php';
            break;
        case 'deleteThisTache':
            require __DIR__ . '/../src/Traitements/deleteThisTache.php';
            break;
        case 'modifTache':
            require __DIR__ . '/../src/Traitements/modifTache.php';
            break;
        case 'modifUser':
            require __DIR__ . '/../src/Traitements/modifUser.php';
            break;
        case 'deleteProfile':
            require __DIR__ . '/../src/Traitements/deleteProfile.php';
            break;
        default:
            break;
    }
}



?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Brief 4 ToDoList</title>
</head>

<body>

    <!-- Page de connexion: -->
    <section id="connexion" class="bg-[url(./image/bgImage.jpg)] bg-opacity-80 bg-cover border-2 border-fuchsia-600 flex min-h-screen items-center justify-center">
        <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
            <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                    Se connecter
                </h3>
            </div>
            <div class="flex flex-col gap-4 p-6">
                <div class="relative h-11 w-full min-w-[200px]">
                    <input id="loginMail" name="loginMail" type="email" required class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="loginMail" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Email
                    </label>
                </div>
                <div class="relative h-11 w-full min-w-[200px]">
                    <input id="loginPassword" name="loginPassword" type="password" autocomplete="current-password" required class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="loginPassword" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Mot de passe
                    </label>
                </div>
            </div>
            <div class="p-6 pt-0">
                <button id="bouttonConnexion" onclick="handleLogin()" class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="submit" data-ripple-light="true">
                    Se connecter
                </button>
                <p class="mt-6 flex justify-center font-sans text-sm font-light leading-normal text-inherit antialiased">
                    Pas encore de compte?
                    <a id="versCreationCompte" href="?section=enregistrer" class="ml-1 block font-sans text-sm font-bold leading-normal text-fuchsia-500 antialiased">
                        S'enregistrer
                    </a>
                </p>
            </div>
        </div>
    </section>

    <!-- Page d'enregistrement -->
    <section id="enregistrer" class="bg-[url(./image/bgImage.jpg)] bg-opacity-80 bg-cover border-2 border-fuchsia-600 flex min-h-screen items-center justify-center">
        <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
            <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                    S'enregistrer
                </h3>
                <?php
                if (isset($code_erreur) && $code_erreur === 0) { ?>
                    <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                        Une erreur est survenue
                    </div>
                <?php } ?>
            </div>
            <div class="flex flex-col gap-4 p-6">
                <div class="relative h-11 w-full min-w-[200px]">
                    <?php
                    if (isset($code_erreur) && $code_erreur === 2) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Veuillez entrer votre nom
                        </div>
                    <?php } ?>
                    <?php
                    if (isset($code_erreur) && $code_erreur === 11) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Votre nom doit faire entre 3 et 50 charactères
                        </div>
                    <?php } ?>
                    <input id="addUserLastName" name="addUserLastName" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="addUserLastName" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Nom
                    </label>
                </div>
                <div class="relative h-11 w-full min-w-[200px]">
                    <?php
                    if (isset($code_erreur) && $code_erreur === 1) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Veuillez entrer votre prénom
                        </div>
                    <?php } ?>
                    <?php
                    if (isset($code_erreur) && $code_erreur === 10) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Votre prénom doit faire entre 3 et 50 charactères
                        </div>
                    <?php } ?>
                    <input id="addUserFirstName" name="addUserFirstName" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="addUserFirstName" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Prenom
                    </label>
                </div>
                <div class="relative h-11 w-full min-w-[200px]">
                    <?php
                    if (isset($code_erreur) && $code_erreur === 3) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Veuillez entrer un mail valide
                        </div>
                    <?php } ?>
                    <?php
                    if (isset($code_erreur) && $code_erreur === 6) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Cette e-mail existe déjà
                        </div>
                    <?php } ?>
                    <?php
                    if (isset($code_erreur) && $code_erreur === 9) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Votre e-mail doit faire entre 3 et 80 charactères
                        </div>
                    <?php } ?>
                    <input id="addUserMail" name="addUserMail" type="email" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="addUserMail" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Email
                    </label>
                </div>
                <div class="relative h-11 w-full min-w-[200px]">
                    <?php
                    if (isset($code_erreur) && $code_erreur === 4) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Veuillez entrer un mot de passe valide
                        </div>
                    <?php } ?>
                    <?php
                    if (isset($code_erreur) && $code_erreur === 8) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Votre mot de passe doit faire au minimum 7 charactères
                        </div>
                    <?php } ?>
                    <input id="addUserPassword1" name="addUserPassword1" type="password" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="addUserPassword1" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Mot de passe
                    </label>
                </div>
                <div class="relative h-11 w-full min-w-[200px]">
                    <?php
                    if (isset($code_erreur) && $code_erreur === 5) { ?>
                        <div class="border border-red-800/10 flex relative *:relative *:size-auto *:m-auto size-auto rounded-lg dark:bg-gray-900 dark:border-white/15 before:rounded-[7px] before:absolute before:inset-0 before:border-t before:border-white before:from-red-400 dark:before:border-white/20 before:bg-gradient-to-b dark:before:from-white/10 dark:before:to-transparent before:shadow dark:before:shadow-gray-950 text-2xl font-bold text-gray-900 dark:text-white text-center">
                            Les mots de passe ne sont pas identiques
                        </div>
                    <?php } ?>
                    <input id="addUserPassword2" name="addUserPassword2" type="password" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                    <label for="addUserPassword2" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                        Mot de passe
                    </label>
                </div>
            </div>
            <div class="p-6 pt-0">
                <button id="bouttonSignUp" onclick="handleAddUser()" name="addUser" class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="submit" data-ripple-light="true">
                    S'enregistrer
                </button>
            </div>
        </div>
    </section>

    <!-- Page tableau de bord -->
    <section id="board" class="border-2 border-fuchsia-600 flex min-h-screen items-center justify-center">
        <!-- component -->
        <div class="h-screen w-full bg-white relative flex overflow-hidden">
            <!-- Sidebar -->
            <aside class="h-full w-20 flex flex-col space-y-10 items-center justify-center relative bg-fuchsia-600 text-white">
                <button id="openModifProfile" class="flex flex-col items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white  hover:duration-300 hover:ease-linear focus:bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z" clip-rule="evenodd" />
                    </svg>
                    <span class="mt-0">Profile</span>
                </button>
                <button id="openEnregistrerTache" class="flex-col flex items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white  hover:duration-300 hover:ease-linear focus:bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                    </svg>
                    <span class="mt-0">Nouvelle</span>
                    <span class="mt-0">tâche</span>
                </button>
                <button id="openEnregistrerCategorie" class="flex-col flex items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white  hover:duration-300 hover:ease-linear focus:bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
                    </svg>
                    <span class="mt-0">Nouvelle</span>
                    <span class="mt-0">catégorie</span>
                </button>
            </aside>

            <div class="w-full h-full flex flex-col justify-between">

                <header class="h-16 w-full flex items-center relative justify-end px-5 space-x-10 bg-fuchsia-600">

                    <div class="flex flex-shrink-0 items-center space-x-4 text-white">

                    <button id="openDeleteProfile" class="flex-col flex items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white  hover:duration-300 hover:ease-linear focus:bg-white">
                        <svg class="h-5 w-5 text-fuchsia-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <line x1="4" y1="7" x2="20" y2="7" />  <line x1="10" y1="11" x2="10" y2="17" />  <line x1="14" y1="11" x2="14" y2="17" />  <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />  <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                        <span>Suppression</span>
                        <span>compte</span>
                    </button>
                        <div class="flex flex-col items-end ">

                            <div class="text-md font-medium ">
                                Bonjour <?php
                                        if (isset($_SESSION['connecté'])) {
                                        ?> <?= $user->getUserLastname(); ?> <?php
                                                                } ?> <?php
                                    if (isset($_SESSION['connecté'])) {
                                    ?> <?= $user->getUserFirstname(); ?><?php
                                                                } ?>
                            </div>
                            <button id="deconnexion" onclick="handleDisconnect()" class="text-sm font-regular">
                                Se déconnecter
                            </button>
                        </div>
                    </div>
                </header>

                <main class="bg-[url(./image/bgImage.jpg)] bg-opacity-80 bg-cover max-w-full h-full flex relative overflow-y-hidden">
                    <div id="afficheTaches" class="h-full w-full m-4 flex flex-wrap items-start justify-start rounded-tl grid-flow-col auto-cols-max gap-4 overflow-y-scroll">
                        <?php
                        if (isset($_SESSION['connecté']) && isset($_SESSION['user'])) {
                            foreach ($tachesListe as $tache) {
                                include __DIR__ . '/../src/Views/todolist.php';
                            }
                        } ?>
                    </div>
                </main>

                <!-- Modal de suppression de compte -->
                <dialog id="modalDeleteProfile" autofocus>
                    <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
                        <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                            <button id="closeDeleteProfile" class="bg-fuchsia-300 hover:bg-fuchsia-500 cursor-pointer hover:text-gray-300 font-sans text-gray-500 w-6 h-6 flex items-center justify-center rounded-full">
                                X
                            </button>
                            <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                                Attention
                            </h3>
                        </div>
                        <button onclick="handleDeleteProfile(<?php
                                        if (isset($_SESSION['connecté'])) {
                                        ?> <?= $user->getUserId(); ?>
                                        <?php } ?>)" class="flex-col flex items-center justify-center rounded-lg cursor-pointer hover:text-gray-800 hover:bg-white  hover:duration-300 hover:ease-linear focus:bg-white">
                            <svg class="h-5 w-5 text-fuchsia-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <line x1="4" y1="7" x2="20" y2="7" />  <line x1="10" y1="11" x2="10" y2="17" />  <line x1="14" y1="11" x2="14" y2="17" />  <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />  <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                            <span>Supprimer</span>
                            <span>définitivement</span>
                        </button>
                    </div>
                </dialog>


                <!-- Modal pour modifier le profile -->
                <dialog id="modifProfile" autofocus>
                    <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
                        <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                            <button id="closeModifProfile" class="bg-fuchsia-300 hover:bg-fuchsia-500 cursor-pointer hover:text-gray-300 font-sans text-gray-500 w-6 h-6 flex items-center justify-center rounded-full">
                                X
                            </button>
                            <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                                Modifier mon profile
                            </h3>
                        </div>
                        <div class="flex flex-col gap-4 p-6">
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="modifUserLastName" name="modifUserLastName" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Nom
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="modifUserFirstName" name="modifUserFirstName" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Prenom
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="modifUserMail" name="modifUserMail" type="email" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Email
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="modifUserPassword" name="modifUserPassword" type="password" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Mot de passe
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input type="password" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Mot de passe
                                </label>
                            </div>
                        </div>
                        <div class="p-6 pt-0">
                            <button onclick="handleModifProfile (<?php if (isset($_SESSION['connecté'])) {?> 
                            <?= $user->getUserId(); ?> 
                            <?php } ?>)" class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="button" data-ripple-light="true">
                                Enregistrer
                            </button>
                        </div>
                    </div>
                </dialog>


                <!-- Modal pour modifier une tache -->
                <dialog class="modifTache" autofocus>
                    <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
                        <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                            <button id="closeModifTache" class="bg-fuchsia-300 hover:bg-fuchsia-500 cursor-pointer hover:text-gray-300 font-sans text-gray-500 w-6 h-6 flex items-center justify-center rounded-full">
                                X
                            </button>
                            <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                                Modifier la tâche
                            </h3>
                        </div>
                        <div class="flex flex-col gap-4 p-6">
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input type="text" class="modifTacheTitle peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Titre
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input type="text" class="modifTacheDescription peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Description
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input type="date" class="modifTacheDate peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Date
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <select class="modifTachePriority peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" ">
                                    <option value="1">Normal</option>
                                    <option value="2">Important</option>
                                    <option value="3">Urgent</option>
                                </select>
                                <label class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Priorité:
                                </label>
                            </div>
                        </div>
                        <div class="p-6 pt-0">
                            <button onclick="handleModifTache (
                                <?php 
                                if (isset($_SESSION['connecté']) && isset($tache)) { 
                                echo $tache->getTacheId().',' .$tache->getUserId();
                                } ?>)"
                                class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="button" data-ripple-light="true">
                                Enregistrer
                            </button>
                        </div>
                    </div>
                </dialog>



                <!-- Modal pour enregistrer une tache -->
                <dialog id="enregistrerTache" autofocus>
                    <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
                        <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                            <button id="closeEnregistrerTache" class="bg-fuchsia-300 hover:bg-fuchsia-500 cursor-pointer hover:text-gray-300 font-sans text-gray-500 w-6 h-6 flex items-center justify-center rounded-full">
                                X
                            </button>
                            <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                                Nouvelle tâche
                            </h3>
                        </div>
                        <div class="flex flex-col gap-4 p-6">
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="addTacheTitle" name="addTacheTitle" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label for="addTacheTitle" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Titre
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="addTacheDescription" name="addTacheDescription" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label for="addTacheDescription" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Description
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="addTacheDate" name="addTacheDate" type="date" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label for="addTacheDate" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Date
                                </label>
                            </div>
                            <div class="relative h-11 w-full min-w-[200px]">
                                <select id="addTachePriority" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" ">
                                    <option value="1">Normal</option>
                                    <option value="2">Important</option>
                                    <option value="3">Urgent</option>
                                </select>
                                <label for="addTachePriority" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Priorité:
                                </label>
                            </div>
                        </div>
                        <div class="p-6 pt-0">
                            <button onclick="handleAddTache(
                                <?php if (isset($_SESSION['connecté'])) { ?> 
                                    <?= $user->getUserId(); ?>
                                <?php } ?>)" class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="button" data-ripple-light="true">
                                Enregistrer
                            </button>
                        </div>
                    </div>
                </dialog>


                <!-- Modal pour enregistrer une categorie -->
                <dialog id="enregistrerCategorie" autofocus>
                    <div class="relative flex w-96 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md mt-10 ml-10">
                        <div class="relative mx-4 -mt-6 mb-4 grid h-28 place-items-center overflow-hidden rounded-xl bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 bg-clip-border text-white shadow-lg shadow-fuchsia-500/40">
                            <button id="closeEnregistrerCategorie" class="bg-fuchsia-300 hover:bg-fuchsia-500 cursor-pointer hover:text-gray-300 font-sans text-gray-500 w-6 h-6 flex items-center justify-center rounded-full">
                                X
                            </button>
                            <h3 class="block font-sans text-3xl font-semibold leading-snug tracking-normal text-white antialiased">
                                Nouvelle catégorie
                            </h3>
                        </div>
                        <div class="flex flex-col gap-4 p-6">
                            <div class="relative h-11 w-full min-w-[200px]">
                                <input id="addCategoryName" name="addCategoryName" type="text" class="peer h-full w-full rounded-md border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-3 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2 focus:border-fuchsia-500 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" placeHolder=" " />
                                <label for="addCategoryName" class="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[4.1] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-fuchsia-500 peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:!border-fuchsia-500 peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:!border-fuchsia-500 peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500">
                                    Nom
                                </label>
                            </div>
                        </div>
                        <div class="p-6 pt-0">
                            <button onclick="handleAddCategory ()" class="block w-full select-none rounded-lg bg-gradient-to-tr from-fuchsia-600 to-fuchsia-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-fuchsia-500/20 transition-all hover:shadow-lg hover:shadow-fuchsia-500/40 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none" type="button" data-ripple-light="true">
                                Enregistrer
                            </button>
                        </div>
                    </div>
                </dialog>
            </div>
        </div>
    </section>

</body>

<script src="https://cdn.tailwindcss.com"></script>
<script src="./js/login.js"></script>
<script src="./js/disconnect.js"></script>
<script src="./js/modal.js"></script>
<script src="./js/addUser.js"></script>
<script src="./js/addTache.js"></script>
<script src="./js/addCategory.js"></script>
<script src="./js/navigation.js"></script>
<script src="./js/addCategoryToTache.js"></script>
<script src="./js/deleteThisTache.js"></script>
<script src="./js/modifTache.js"></script>
<script src="./js/modifUser.js"></script>
<script src="./js/deleteProfile.js"></script>

</html>