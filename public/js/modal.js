let openModifProfile = document.getElementById('openModifProfile');
let modalModifProfile = document.getElementById('modifProfile');
let closeModifProfile = document.getElementById('closeModifProfile');

openModifProfile.addEventListener("click", () => {
    modalModifProfile.showModal();
})
closeModifProfile.addEventListener("click", () => {
    modalModifProfile.close();
})

let openEnregistrerTache = document.getElementById('openEnregistrerTache');
let modalEnregistrerTache = document.getElementById('enregistrerTache');
let closeEnregistrerTache = document.getElementById('closeEnregistrerTache');

openEnregistrerTache.addEventListener("click", () => {
    modalEnregistrerTache.showModal();
})
closeEnregistrerTache.addEventListener("click", () => {
    modalEnregistrerTache.close();
})


let openModifTaches = document.querySelectorAll('.openModifTache');
let modalModifTache = document.querySelector('.modifTache');
let closeModifTache = document.getElementById('closeModifTache');

openModifTaches.forEach(openModifTache => {
    openModifTache.addEventListener("click", () => {
        modalModifTache.showModal();
    });
});
closeModifTache.addEventListener("click", () => {
    modalModifTache.close();
})


let openEnregistrerCategorie = document.getElementById('openEnregistrerCategorie');
let modalEnregistrerCategorie = document.getElementById('enregistrerCategorie');
let closeEnregistrerCategorie = document.getElementById('closeEnregistrerCategorie');

openEnregistrerCategorie.addEventListener("click", () => {
    modalEnregistrerCategorie.showModal();
})
closeEnregistrerCategorie.addEventListener("click", () => {
    modalEnregistrerCategorie.close();
})

let openDeleteProfile = document.getElementById('openDeleteProfile');
let modalDeleteProfile = document.getElementById('modalDeleteProfile');
let closeDeleteProfile = document.getElementById('closeDeleteProfile');

openDeleteProfile.addEventListener("click", () => {
    modalDeleteProfile.showModal();
})
closeDeleteProfile.addEventListener("click", () => {
    modalDeleteProfile.close();
})