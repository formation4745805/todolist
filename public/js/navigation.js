// Les  bouttons et liens d'action:
let versCreationCompte = document.getElementById('versCreationCompte');
let bouttonSignUp = document.getElementById('bouttonSignUp');
let bouttonConnexion = document.getElementById('bouttonConnexion');
let deconnexion = document.getElementById('deconnexion');

// Les sections à animer:
let sectionConnexion = document.getElementById('connexion');
let sectionEnregistrer = document.getElementById('enregistrer');
let sectionBoard = document.getElementById('board');

//Les écoute d'évènement:
versCreationCompte.addEventListener('click', ouvrirSection());
bouttonSignUp.addEventListener('click', ouvrirSection());
// bouttonConnexion.addEventListener('click', ouvrirSection());
deconnexion.addEventListener('click', ouvrirSection());

// La fonction à jouer
function ouvrirSection(){
    let urlSearch = new URLSearchParams(window.location.search);
    let resultSearch = urlSearch.get("section");
    if (resultSearch == "enregistrer") {
        sectionConnexion.classList.add("hidden");
        sectionEnregistrer.classList.remove("hidden");
        sectionBoard.classList.add("hidden");
    }else if (resultSearch == "connexion") {
        sectionConnexion.classList.remove("hidden");
        sectionEnregistrer.classList.add("hidden");
        sectionBoard.classList.add("hidden");
    }else if (resultSearch == "board"){
        sectionConnexion.classList.add("hidden");
        sectionEnregistrer.classList.add("hidden");
        sectionBoard.classList.remove("hidden");
    }else {
        sectionConnexion.classList.remove("hidden");
        sectionEnregistrer.classList.add("hidden");
        sectionBoard.classList.add("hidden");
    }
}

//Jouer à l'ouverture:
window.onload = ouvrirSection ();