function handleDeleteProfile(id){

    let deleteCredentials = {
        UserId: id
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(deleteCredentials),
    };

    fetch("/index.php?req=deleteProfile", params)
        .then((res) => res.text())
        .then((data)=>{
            console.log(data);
            window.location.href = '/?section=connexion';
        });
}