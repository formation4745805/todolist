function deleteThisTache(id){

    let deleteCredential = {
        TacheId : id
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(deleteCredential),
    };

    fetch("/index.php?req=deleteThisTache", params)
        .then((res) => res.text())
        .then((data)=>console.log(data));
}