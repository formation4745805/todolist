function handleAddTache(id) {

    let addTacheTitle = document.getElementById('addTacheTitle').value;
    let addTacheDescription = document.getElementById('addTacheDescription').value;
    let addTacheDate = document.getElementById('addTacheDate').value;
    let addTachePriority = document.getElementById('addTachePriority').value;
    let modalEnregistrerTache = document.getElementById('enregistrerTache');


    let tacheCredentials = {
        TacheTitle : addTacheTitle,
        TacheDescription: addTacheDescription,
        TacheDate: addTacheDate,
        UserId: id,
        PriorityId: addTachePriority
    };

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(tacheCredentials),
    };

    fetch("/index.php?req=addTache", params)
        .then((res) => res.text())
        .then((data) => {
            addTacheTitle = "";
            addTacheDescription = "";
            addTacheDate = "";
            addTachePriority = "";
            modalEnregistrerTache.close();

            
        }).catch((error)=>{
            alert('Erreur lors de l\'enregistrement de la tâche :', error);
        });
}
