function handleModifProfile (id){

    let modalModifProfile = document.getElementById('modifProfile');
    let modifUserLastName = document.getElementById('modifUserLastName').value;
    let modifUserFirstName = document.getElementById('modifUserFirstName').value;
    let modifUserMail = document.getElementById('modifUserMail').value;
    let modifUserPassword = document.getElementById('modifUserPassword').value;

    let modifUserProfile = {
        UserId : id,
        UserLastName: modifUserLastName,
        UserFirstName: modifUserFirstName,
        UserMail: modifUserMail,
        UserPassword: modifUserPassword,
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(modifUserProfile),
    };

    fetch("/index.php?req=modifUser", params)
        .then((res) => res.text())
        .then((data) => {
            modifUserLastName = "";
            modifUserFirstName = "";
            modifUserMail = "";
            modifUserPassword = "";
            modalModifProfile.close();
            console.log(data)
        });
}