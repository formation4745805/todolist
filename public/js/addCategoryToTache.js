function handleCategoryToTache(id) {

    let addCategoryToThisTache = document.querySelector('.addCategoryToThisTache').value;

    let categoryToThisTache = {
        TacheId: id,
        CategoryId: addCategoryToThisTache
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(categoryToThisTache),
    };

    fetch("/index.php?req=addOption", params)
        .then((res) => res.text())
        .then((data) => {
            console.log(data);
        });
}