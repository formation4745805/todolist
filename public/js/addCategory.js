function handleAddCategory (){
    let addCategoryName = document.getElementById('addCategoryName').value;
    let moadlEnregistrerCategorie = document.getElementById('enregistrerCategorie');

    let categoryCredentials = {
        CategoryName : addCategoryName
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(categoryCredentials),
    };

    fetch("/index.php?req=addCategory", params)
        .then((res) => res.text())
        .then((data) => {
            moadlEnregistrerCategorie.close();
            addCategoryName.value = "";
        });
}