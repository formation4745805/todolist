function handleModifTache(TacheId, UserId){

    let modalModifTache = document.querySelector('.modifTache');
    let modifTacheTitle = document.querySelector('.modifTacheTitle').value;
    let modifTacheDescription = document.querySelector('.modifTacheDescription').value;
    let modifTacheDate = document.querySelector('.modifTacheDate').value;
    let modifTachePriority = document.querySelector('.modifTachePriority').value;

    let modifCredentials = {
        TacheId: TacheId,
        TacheTitle: modifTacheTitle,
        TacheDescription: modifTacheDescription,
        TacheDate: modifTacheDate,
        UserId: UserId,
        PriorityId: modifTachePriority
    }

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(modifCredentials),
    };

    fetch("/index.php?req=modifTache", params)
        .then((res) => res.text())
        .then((data) => {
            modifTacheTitle = "";
            modifTacheDescription = "";
            modifTacheDate = "";
            modifTachePriority = "";
            modalModifTache.close();
            console.log(data)
        });
}