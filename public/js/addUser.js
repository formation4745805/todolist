function handleAddUser() {

    let addUserLastName = document.getElementById('addUserLastName').value;
    let addUserFirstName = document.getElementById('addUserFirstName').value;
    let addUserMail = document.getElementById('addUserMail').value;
    let addUserPassword1 = document.getElementById('addUserPassword1').value;
    let addUserPassword2 = document.getElementById('addUserPassword2').value;

    if(addUserLastName.length > 3 && addUserLastName.length < 50) {
      if(addUserFirstName.length > 3 && addUserFirstName.length < 50) {
        if(addUserMail.length > 3 && addUserMail.length < 80) {
          if(addUserPassword1.length > 7) {
            if(addUserPassword1 == addUserPassword2){

            let userCrendentials = {
              UserLastName: addUserLastName,
              UserFirstName: addUserFirstName,
              UserMail: addUserMail,
              UserPassword: addUserPassword1,
            };

            let params = {
                method: "POST",
                headers: {
                  "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(userCrendentials),
            };

              fetch("/index.php?req=register", params)
              .then((res) => res.text())
              .then((data) => console.log(data));
          }else{
            window.location.href = '/?section=enregistrer&&erreur=ERREUR_MDP_IDENTIQUE';
          }
        }else{
          window.location.href = '/?section=enregistrer&&erreur=ERREUR_MDP_TAILLE';
        }
      }else{
        window.location.href = '/?section=enregistrer&&erreur=ERREUR_EMAIL_TAILLE';
      }
    }else{
      window.location.href = '/?section=enregistrer&&erreur=ERREUR_FIRSTNAME_TAILLE';
    }
  }else{
    window.location.href = '/?section=enregistrer&&erreur=ERREUR_LASTNAME_TAILLE';
  }

  window.location.href = '/?section=connexion'
}

