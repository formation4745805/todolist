function handleLogin() {

    let loginMail = document.getElementById('loginMail').value;
    let loginPassword = document.getElementById('loginPassword').value;

    let emailCrendentials = {
        UserMail: loginMail,
        UserPassword: loginPassword,
    };

    let params = {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(emailCrendentials),
    };

    fetch("/index.php?req=login", params)
        .then((res) => res.text())
        .then((data) => {
            window.location.href = '/?section=board';
        }).catch((error)=>{
            console.log(error);
            window.location.href = '/?section=connexion'
        });
}




